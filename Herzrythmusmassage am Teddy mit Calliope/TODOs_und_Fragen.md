# Unsere Aufgaben
Hier werden alle ausstehenden Aufgaben gelistet und Fragen gesammelt, die hier 
oder im persönlichem Gespräch beantwortet werden. Dinge, die beantwortet oder 
bearbeitet wurden werden als **DONE** markiert.

## Fragen
- Werden die Drucksensoren **in** die Teddys gelegt oder liegen sie auf?
- Können die Kinder vorher schon Variablen? Wenn nicht müssen wir Zeit einrechnen, um das Konzept einzuführen.
- Bleibt Calliope die ganze Zeit über das USB Kabel am Computer oder bekommen die Kinder Powerbanks? Ich finde es besser, wenn der Teddy quasi mit dem Computer (über den Drucksensor und Calliope) verbunden ist, damit es noch ersichtlicher ist, dass wirklich die Kinder mit ihren Programmen die Kontrolle auf Calliope über den Drucksensor am Teddy haben.

## TODO
- Vorbereitung von den Drucksensoren und Calliope (und testen, ob sie funktionieren?)
- Gestaltung der _Dekoecke_ mit Puppe und Plakaten
- Termin für Besprechung festlegen, bei dem wir konkrete Aufgaben verteilen.
